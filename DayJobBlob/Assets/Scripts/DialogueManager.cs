﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class DialogueManager : MonoBehaviour {

    public GameObject dialogueBox;
    public CinemachineFreeLook npcCamera;
    public Text nameText;
    public Text dialogueText;
    public string currentSentence;

    public float textDelay;

    public Queue<string> sentences;

    public bool isTyping;

	// Use this for initialization
	void Start () {
        sentences = new Queue<string>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartDialogue(Dialogue dialogue)
    {
        dialogueBox.SetActive(true);
        nameText.text = dialogue.name;

        sentences.Clear();

        foreach(string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (isTyping)
        {
            StopAllCoroutines();
            dialogueText.text = currentSentence;
            isTyping = false;
            return;
        }

        if (sentences.Count == 0 )
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        currentSentence = sentence;

        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence (string sentence)
    {
        isTyping = true;

        dialogueText.text = "";

        StringBuilder builder = new StringBuilder();

        foreach (char letter in sentence.ToCharArray()) {
            builder.Append(letter);
            dialogueText.text = builder.ToString();
            yield return new WaitForSeconds(textDelay);
        }

        yield return new WaitForEndOfFrame();
        isTyping = false;

    }

    void EndDialogue()
    {
        dialogueBox.SetActive(false);
        npcCamera.m_Priority = 1;
        npcCamera.LookAt = null;

    }

}
 