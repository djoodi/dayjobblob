﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DialogueTrigger : MonoBehaviour {

    public Dialogue dialogueMeeting;
    public Dialogue dialogueQuestInProgress;
    public Dialogue dialogueQuestTurnIn;
    public Dialogue dialoguePostTurnIn;
    public CinemachineFreeLook npcCamera;

    DialogueManager theDM;


    private void Awake()
    {
        theDM = FindObjectOfType<DialogueManager>();
    }

    public void TriggerDialogue()
    {
        
        theDM.StartDialogue(dialogueMeeting);
        

        npcCamera.LookAt = gameObject.transform;
        npcCamera.Priority = 20;
    }
}
