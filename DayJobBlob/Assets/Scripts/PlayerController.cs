﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;
using RoboRyanTron.Unite2017.Events;
using UnityEngine.Audio;

public class PlayerController : MonoBehaviour
{ 
    private Rigidbody2D theRB;
    private SpriteMeshInstance smInstance;
    private BoxCollider2D theBox;
    private bool touchingWall;
    private float countdown;
   
    public bool canMorph;
    public float timeStuck;
    public GameEvent camShake;
    public GameEvent isStuck;
    public bool isCircle;
    public float tippingThreshold;
    public float canMoveThreshold;
    public float camShakeThreshold;
    public bool canMove;
    public bool canRaise;
    public SpriteMesh squareMesh;
    public SpriteMesh circleMesh;
    public SpriteMesh[] meshes;
    
    public float rollSpeed;

    // Start is called before the first frame update
    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        smInstance = GetComponent<SpriteMeshInstance>();
        theBox = GetComponent<BoxCollider2D>();
        canRaise = false;

        countdown = timeStuck;

        if (isCircle)
        {
            BecomeCircle();
        }
        else
        {
            smInstance.spriteMesh = squareMesh;
            theBox.enabled = true;
            //BecomeSquare();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetInputs();
        CheckIfStuck(transform.rotation.eulerAngles.z);
    }

    void GetInputs()
    {
        float movement = Input.GetAxisRaw("Horizontal");
        
        if (isCircle)
        {
            theRB.angularVelocity = -movement * rollSpeed;
        } else
        {
            if (IsTippingOver(transform.rotation.eulerAngles.z) == false && canMove)
            theRB.angularVelocity = -movement * rollSpeed;
        }


        if (Input.GetButtonDown("Shift"))
        {
            SwitchForm();
        }
    }

    public void SwitchForm()
    {
        if (canMorph)
        {
            isCircle = !isCircle;

            if (isCircle)
            {
                BecomeCircle();
            }
            else
            {
                BecomeSquare();
            }
        }

    }

    public void BecomeCircle()
    {
        //smInstance.spriteMesh = circleMesh;
        StartCoroutine(Circle());
        //bones.SetActive(true);
        theBox.enabled = false;
        isCircle = true;
    }

    IEnumerator Circle()
    {
        int i = 15;
        while (smInstance.spriteMesh != circleMesh)
        {
            smInstance.sortingLayerName = "BG";
            smInstance.spriteMesh = meshes[i];

            i--;

            yield return null;
        }

        smInstance.sortingLayerName = "Player";


    }

    public void BecomeSquare()
    {
        //smInstance.spriteMesh = squareMesh;
        StartCoroutine(Square());
        //bones.SetActive(false);
        theBox.enabled = true;
        canMove = true;
        isCircle = false;
    }
    
    IEnumerator Square()
    {
        int i = 0;
        while (smInstance.spriteMesh != squareMesh)
        {
            smInstance.sortingLayerName = "BG";
            smInstance.spriteMesh = meshes[i];

            i++;

            yield return null;

        }
        smInstance.sortingLayerName = "Player";
    }
    

    bool IsTippingOver(float x)
    {
        float corner1 = 45;
        float corner2 = 135;
        float corner3 = 225;
        float corner4 = 315;


        float corner5 = 0;
        float corner6 = 90;
        float corner7 = 180;
        float corner8 = 270;
        float corner9 = 360;

        bool y;


        if (x > corner1 - tippingThreshold && x < corner1 + tippingThreshold)
        {
            y = true;
            canMove = false;
            canRaise = true;
        }
        else if (x > corner2 - tippingThreshold && x < corner2 + tippingThreshold)
        {
            y = true;
            canMove = false;
            canRaise = true;
        }
        else if (x > corner3 - tippingThreshold && x < corner3 + tippingThreshold)
        {
            y = true;
            canMove = false;
            canRaise = true;
        }
        else if (x > corner4 - tippingThreshold && x < corner4 + tippingThreshold)
        {
            y = true;
            canMove = false;
            canRaise = true;
        }
        else
        {
            y = false;
        }

        if (x > corner5 - canMoveThreshold && x < corner5 + canMoveThreshold)
        {
            canMove = true;
        }
        else if (x > corner6 - canMoveThreshold && x < corner6 + canMoveThreshold)
        {
            canMove = true;
        }
        else if (x > corner7 - canMoveThreshold && x < corner7 + canMoveThreshold)
        {
            canMove = true;
        }
        else if (x > corner8 - canMoveThreshold && x < corner8 + canMoveThreshold)
        {
            canMove = true;
        }
        else if (x > corner9 - canMoveThreshold && x < corner9 + canMoveThreshold)
        {
            canMove = true;
        }

        if (canRaise)
        {
            if (x > corner5 - camShakeThreshold && x < corner5 + camShakeThreshold)
            {
                camShake.Raise();
                canRaise = false;
            }
            else if (x > corner6 - camShakeThreshold && x < corner6 + camShakeThreshold)
            {
                camShake.Raise();
                canRaise = false;
            }
            else if (x > corner7 - camShakeThreshold && x < corner7 + camShakeThreshold)
            {
                camShake.Raise();
                canRaise = false;
            }
            else if (x > corner8 - camShakeThreshold && x < corner8 + camShakeThreshold)
            {
                camShake.Raise();
                canRaise = false;
            }
            else if (x > corner9 - camShakeThreshold && x < corner9 + camShakeThreshold)
            {
                camShake.Raise();
                canRaise = false;
            }
        }
        /*
        if (theRB.velocity.x > -.95f && theRB.velocity.x < 0.05f)
        {
            canMove = true;
        }
        */


        return y;
    }

    void CheckIfStuck(float x)
    {
        
        float corner5 = 0;
        float corner6 = 90;
        float corner7 = 180;
        float corner8 = 270;
        float corner9 = 360;
        

        if (theRB.velocity.x > -.95f && theRB.velocity.x < 0.05f && !isCircle)
        {

            //Debug.Log("Raising Stuck!");

            if (countdown > 0)
            {
                if (x > corner5 + canMoveThreshold && x < corner6 - canMoveThreshold)
                {
                    //isStuck.Raise();
                    countdown -= Time.deltaTime;
                }
                else if (x > corner6 + canMoveThreshold && x < corner7 - canMoveThreshold)
                {
                    countdown -= Time.deltaTime;
                    //isStuck.Raise();
                }
                else if (x > corner7 + canMoveThreshold && x < corner8 - canMoveThreshold)
                {
                    countdown -= Time.deltaTime;
                    //isStuck.Raise();
                }
                else if (x > corner8 + canMoveThreshold && x < corner9 - canMoveThreshold)
                {
                    countdown -= Time.deltaTime;
                    //isStuck.Raise();
                }
            }



            if (countdown < 0)
            {
                isStuck.Raise();
            }
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            touchingWall = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Wall")
        {
            touchingWall = true;
            Debug.Log("wall");
            canMove = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            touchingWall = false;
        }
    }
}
