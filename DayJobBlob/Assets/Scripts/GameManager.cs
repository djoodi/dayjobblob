﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public bool seenOcto;
    public bool hasBeenToBathroom;

    public string previousScene;
    public string currentScene;

    public int timesCaughtBlob;

    public float rateLerp;

    public AudioMixer mixer;
    public AudioMixerGroup[] loopingAudio;


    public AudioSource[] office;
    public AudioSource[] bathroom;
    public AudioSource[] city;
    public AudioSource[] home;
    public AudioSource rainLite;
    public AudioSource forest;

    void Awake()
    {
        Debug.Log("Awake");
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }

    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        Debug.Log(mode);
        Debug.Log("CheckingScene");
        currentScene = scene.name;
        CheckScene();


    }

    private void Start()
    {
        Debug.Log("Start");
    }

    void OnDisable()
    {
        Debug.Log("OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    // Update is called once per frame
    void Update()
    {

    }

    

    public void BlobCaught()
    {
        timesCaughtBlob++;
    }

    public void ChangeBGM(AudioMixerGroup group)
    {
        SilenceGroups();
        mixer.SetFloat(group.name, 0f);

        if (group.name.ToLower() == "office")
        {
            foreach (AudioSource s in office)
            {
                s.Play();
            }
        } else if (group.name.ToLower() == "bathroom")
        {
            foreach (AudioSource s in bathroom)
            {
                s.Play();
            }
        } else if (group.name.ToLower() == "city")
        {
            foreach (AudioSource s in city)
            {
                s.Play();
            }
        } else if (group.name.ToLower() == "home")
        {
            foreach(AudioSource s in home)
            {
                s.Play();
            }
        }
    }

    public void SilenceGroups()
    {
        foreach (AudioMixerGroup g in loopingAudio)
        {
            mixer.SetFloat(g.name, -80f);

            foreach (AudioSource s in office)
            {
                s.Stop();
            }
            foreach (AudioSource s in bathroom)
            {
                s.Stop();
            }
            foreach (AudioSource s in city)
            {
                s.Stop();
            }
            foreach (AudioSource s in home)
            {
                s.Stop();
            }
        }
    }

    public void CheckScene()
    {
        Debug.Log(currentScene);
        if (currentScene == "Office1" || currentScene == "Office2")
        {
            mixer.SetFloat("Office", 0f);
            if (!office[0].isPlaying)
                ChangeBGM(loopingAudio[0]);
            Debug.Log("Changing music to office");
        } else if (currentScene == "Bathroom")
        {
            mixer.SetFloat("Office", -20f);
            //ChangeBGM(loopingAudio[1]);
        } else if (currentScene == "City1" || currentScene == "HouseExterior")
        {
            Debug.Log("Playing city");
            //ChangeBGM(loopingAudio[2]);
            mixer.SetFloat("Office", -80f);
            mixer.SetFloat("City", 0f);
            if (!city[1].isPlaying)
            {
                foreach (AudioSource s in city)
                {
                    Debug.Log("playing" + s.name);
                    s.Play();
                }
            }

            if (currentScene == "HouseExterior")
            {
                Debug.Log("Transitioning rain");
                rainLite.Play();
                rainLite.volume = .3f;

                city[1].Stop();
                city[2].Stop();
                city[3].Stop();
            }


            foreach (AudioSource s in office)
            {
                s.Stop();
            }
        }
    }

    IEnumerator Transition(AudioSource x, AudioSource y)
    {
        while (x.volume != 0)
        {
            x.volume -= rateLerp;
            y.volume += rateLerp;

            yield return null;
        }

    }

    public void CityToHome()
    {
        //turn off busy city noises
        StartCoroutine(FadeVolume(city[0]));
        StartCoroutine(FadeVolume(rainLite));

        forest.Play();
        forest.volume = 0;
        StartCoroutine(IncreaseVolume(forest, .5f));
    }

    IEnumerator FadeVolume(AudioSource x)
    {
        while (x.volume != 0)
        {
            x.volume -= rateLerp*Time.deltaTime;

            yield return null;
        }
    }

    IEnumerator IncreaseVolume(AudioSource x, float target)
    {
        while (x.volume < target)
        {
            x.volume += rateLerp*Time.deltaTime;

            yield return null;
        }
    }
}
