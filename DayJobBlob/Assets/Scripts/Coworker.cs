﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Events;

public class Coworker : MonoBehaviour
{
    public GameEvent forceSquare;
    public GameEvent attemptedBlob;

    public ThoughtTrigger caughtBlurb;

    private PlayerController thePC;

    // Start is called before the first frame update
    void Start()
    {
        thePC = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            thePC.canMorph = false;
            if (Input.GetButtonDown("Shift"))
            {
                attemptedBlob.Raise();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            {
                if (thePC.isCircle)
                {
                    forceSquare.Raise();
                    caughtBlurb.TriggerDialogue();
                }
            }

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            thePC.canMorph = true;
        }
    }


}
