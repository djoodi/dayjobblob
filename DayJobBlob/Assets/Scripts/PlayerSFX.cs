﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerSFX : MonoBehaviour
{
    public AudioSource[] blobWalks;
    public AudioSource[] squareWalks;
    public AudioSource toSquare;
    public AudioSource toCircle;

    public void BlobWalk()
    {
        int x = Mathf.FloorToInt(Random.Range(0, blobWalks.Length));

        blobWalks[x].Play();
    }

    public void SquareWalk()
    {
        int x = Mathf.FloorToInt(Random.Range(0, squareWalks.Length));

        squareWalks[x].Play();
    }

    public void ToSquare()
    {
        toSquare.Play();
    }
    
    public void ToCircle()
    {
        toCircle.Play();
    }
}

