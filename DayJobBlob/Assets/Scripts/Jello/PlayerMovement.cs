﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody2D theRB;

    private Switch switcher;

    private JelloSpringBody jelloScript;

    public PolygonCollider2D blobCollider;

    public float horDir;
    public float moveSpeed;
    public float jumpForce;
    public bool isGrounded;

    // Start is called before the first frame update
    private void Awake()
    {
        theRB = GetComponent<Rigidbody2D>();
        switcher = GetComponent<Switch>();
        jelloScript = GetComponent<JelloSpringBody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetInputs();

        Debug.Log("velocity: " + theRB.velocity.ToString() +  "angular velo: " + theRB.angularVelocity.ToString() + "inertia: " + theRB.inertia.ToString());
    }

    void GetInputs()
    {

        horDir = Input.GetAxisRaw("Horizontal");


        if (horDir == 1 || horDir == -1)
        {
            theRB.angularVelocity = -horDir * moveSpeed;
        } else if (horDir == 0)
        {
            theRB.angularVelocity = 0;
            theRB.velocity = new Vector2(0, 0);
        }

        /*
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("jumping");
            theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
        }
        */
    }
}
