﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSwitch : MonoBehaviour
{
    public PolygonCollider2D col1;
    public PolygonCollider2D col2;

    // Start is called before the first frame update
    private void OnMouseDown()
    {
        col1 =  col1.GetCopyOf<PolygonCollider2D>(col2);
    }
}
