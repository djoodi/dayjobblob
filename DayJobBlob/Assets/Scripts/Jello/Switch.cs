﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public bool isJelly;

    public Texture2D squareSpr;
    public Sprite[] squareSprites;

    public Texture2D circleSpr;
    public Sprite[] circleSprites;



    public PolygonCollider2D blobCollider;
    public PolygonCollider2D jellyCollider; // jelly values
    public PolygonCollider2D squareCollider; // square values

    private JelloSpringBody jelloScript;
    private JelloWorld jelloWorld;
    private SpriteMeshLink spriteLink;



    // Start is called before the first frame update
    private void Awake()
    {
        jelloScript = GetComponent<JelloSpringBody>();
        spriteLink = GetComponent<SpriteMeshLink>();

    }

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (jelloWorld == null)
        {
            jelloWorld = FindObjectOfType<JelloWorld>();
        }

        GetInputs();

    }

    public void SwitchBody()
    {
        isJelly = !isJelly;

        if (isJelly)
        {
            BecomeJelly();
        } else
        {
            BecomeSquare();
        }
    }

    void BecomeJelly()
    {
        spriteLink.SetTextureAndSprites(circleSpr, circleSprites);
        //blobCollider = blobCollider.GetCopyOf(jellyCollider);
        blobCollider.enabled = true;
        squareCollider.enabled = false;
        jelloWorld.enabled = true;
        jelloScript.enabled = true;
    }

    void BecomeSquare()
    {
        jelloScript.enabled = false;
        jelloWorld.enabled = false;
        spriteLink.SetTextureAndSprites(squareSpr, squareSprites);
        blobCollider.enabled = false;
        squareCollider.enabled = true;
        //blobCollider = blobCollider.GetCopyOf(squareCollider);

    }

    void GetInputs()
    {
        if (Input.GetButtonDown("Shift")) {
            SwitchBody();
        }
    }
}


