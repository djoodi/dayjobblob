﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ZoomOutHere : MonoBehaviour
{
    public CinemachineVirtualCamera theCam;
    private float original;

    public float targetOrtho;
    public float rateChange;

    private void Awake()
    {
        original = 10;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ZoomOut();
        }
    }

    public void ZoomOut()
    {
        StartCoroutine(ZoomOutCo(targetOrtho, rateChange));
    }

    IEnumerator ZoomOutCo(float target, float rate)
    {
        while (theCam.m_Lens.OrthographicSize < target)
        {
            theCam.m_Lens.OrthographicSize += rate;
            yield return null;
        }
        //theCam.m_Lens.OrthographicSize = Mathf.Lerp(theCam.m_Lens.OrthographicSize, target, rate * Time.deltaTime);


    }
}
