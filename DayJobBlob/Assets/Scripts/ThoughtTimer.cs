﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThoughtTimer : MonoBehaviour
{
    public ThoughtBubble thoughts;
    public ThoughtTrigger trigger;

    public float timeBetweenThoughts;
    public float countdown;

    // Start is called before the first frame update
    void Start()
    {
        countdown = timeBetweenThoughts;
        trigger.TriggerDialogue();
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown > 0)
        {
            if (thoughts.sentences.Count > 0)
            {
                countdown -= Time.deltaTime;
            }

        }
        else
        {
            thoughts.DisplayNextSentence();
            countdown = timeBetweenThoughts;
        }
    }
}
