﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PenSFX : MonoBehaviour
{
    public AudioSource[] drops;

    public void PlayRandom()
    {
        int x = Mathf.FloorToInt(Random.Range(0, drops.Length));

        drops[x].Play();
    }

}
