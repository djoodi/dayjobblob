﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HasBeenToBathroom : MonoBehaviour
{
    public GameObject disable;
    public GameObject enable;

    private void Start()
    {
        if (GameManager.instance.hasBeenToBathroom)
        {
            disable.SetActive(false);
            enable.SetActive(true);
        } 

        if (SceneManager.GetActiveScene().name == "Bathroom")
        {
            GameManager.instance.hasBeenToBathroom = true;
        }
    }
}
