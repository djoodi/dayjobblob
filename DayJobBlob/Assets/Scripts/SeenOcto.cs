﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeenOcto : MonoBehaviour
{
    public GameObject obj;

    private void Update()
    {
        if (GameManager.instance.seenOcto)
        {
            obj.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.instance.seenOcto = true;
        }
    }
}
