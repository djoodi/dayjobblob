﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairs : MonoBehaviour
{
    public Transform other;
    public bool canTele;

    public GameObject playerObject;
    private PlayerController thePC;
    // Start is called before the first frame update
    void Start()
    {
        playerObject = FindObjectOfType<PlayerController>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (canTele)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Destroy(playerObject);
                Instantiate(playerObject, other.position, other.rotation);
                canTele = false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            canTele = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            canTele = false;
        }
    }
}
