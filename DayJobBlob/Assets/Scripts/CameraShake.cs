﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
    public float duration;
    public float magnitude;

    private CinemachineCameraOffset offsetScript;

    private void Start()
    {
        offsetScript = GetComponent<CinemachineCameraOffset>();
    }
    public void CamShake()
    {
        StartCoroutine("Shake");
    }

    IEnumerator Shake()
    {
        Vector3 originalPos = offsetScript.m_Offset;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            offsetScript.m_Offset = new Vector3(x, y, originalPos.z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        offsetScript.m_Offset = originalPos;
    }
}
