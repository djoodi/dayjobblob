﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pen : MonoBehaviour
{
    public ThoughtTrigger bathroomBreak;
    public float delayUntilDrop;
    private float countdown;
    private Animator anim;
    private CapsuleCollider2D theCol;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delayUntilDrop;
        theCol = GetComponent<CapsuleCollider2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        } else
        {
            anim.SetBool("drop", true);
        }
    }

    public void EnableTrigger()
    {
        theCol.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        theCol.enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        bathroomBreak.TriggerDialogue();
    }
}
