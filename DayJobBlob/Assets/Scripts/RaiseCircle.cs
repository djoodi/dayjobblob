﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Events;

public class RaiseCircle : MonoBehaviour
{
    public GameEvent becomeCircle;
    private bool hasSwitched;

    private void Update()
    {
        if (!hasSwitched)
        {
            becomeCircle.Raise();
            hasSwitched = true;
        }
    }
}
