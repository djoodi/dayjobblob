﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacePlayer : MonoBehaviour
{
    // This script determines which side the player spawns on in the scene depending on what the last scene was.

    public Transform left;
    public Transform right;

    public string leftScene;
    public string rightScene;

    private void Awake()
    {
        if (GameManager.instance.previousScene == leftScene)
        {
            FindObjectOfType<PlayerController>().gameObject.transform.position = left.position;
        } else if (GameManager.instance.previousScene == rightScene)
        {
            FindObjectOfType<PlayerController>().gameObject.transform.position = right.position;
        } else
        {

        }
    }
}
