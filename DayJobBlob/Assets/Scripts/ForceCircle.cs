﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceCircle : MonoBehaviour
{
    private PlayerController thePC;

    private void Awake()
    {
        thePC = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            thePC.BecomeCircle();
            thePC.rollSpeed = 500;
        }
    }
}
