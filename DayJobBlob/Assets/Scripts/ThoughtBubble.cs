﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using RoboRyanTron.Unite2017.Events;
using UnityEngine.UI;

public class ThoughtBubble : MonoBehaviour
{
    private Animator myAnim;

    public GameEvent disableBubble;

    //public GameObject dialogueBox;
    public Text nameText;
    public Text dialogueText;
    public string currentSentence;

    public float textDelay;

    public float timeActive;
    public float countdown;

    public Queue<string> sentences;

    public bool isTyping;

    // Use this for initialization
    void Awake()
    {
        sentences = new Queue<string>();
        myAnim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        } if (countdown < 0.5 && countdown > 0.1)
        {
            EndDialogue();
        }

        if (tag == "Player")
        {
            if (Input.GetButtonDown("Jump"))
            {
                DisplayNextSentence();
            }
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        //dialogueBox.SetActive(true);
        //nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (isTyping)
        {
            StopAllCoroutines();
            dialogueText.text = currentSentence;
            isTyping = false;
            return;
        }

        if (sentences.Count == 1)
        {
            countdown = timeActive;
            //return;
        }

        string sentence = sentences.Dequeue();
        currentSentence = sentence;

        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        isTyping = true;

        dialogueText.text = "";

        StringBuilder builder = new StringBuilder();

        foreach (char letter in sentence.ToCharArray())
        {
            builder.Append(letter);
            dialogueText.text = builder.ToString();
            yield return new WaitForSeconds(textDelay);
        }

        yield return new WaitForEndOfFrame();
        isTyping = false;

    }

    void EndDialogue()
    {
        if (gameObject.tag == "Player")
        {
            disableBubble.Raise();
        }

        if (gameObject.tag == "Coworker")
        {
            MakeInactive();
        }

    }

    public void MakeActive()
    {
        myAnim.SetBool("active", true);
    }

    public void MakeInactive()
    {
        myAnim.SetBool("active", false);
    }

}
