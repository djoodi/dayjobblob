﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Events;
using UnityEngine;

public class ThoughtTrigger : MonoBehaviour
{
    public bool hasTriggered;
    public bool needSquare;
    public bool isTrigger;
    public bool isStuck;
    public Dialogue dialogue;

    private PlayerController thePC;

    public ThoughtBubble theBubble;

    public GameEvent enableBubble;

    private void Start()
    {
        thePC = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (isStuck && !hasTriggered)
        {
            TriggerDialogue();
            hasTriggered = true;
        }
    }

    private void Awake()
    {
        hasTriggered = false;
    }

    public void TriggerDialogue()
    {
        theBubble.StartDialogue(dialogue);
        if (gameObject.tag == "Player")
        {
            enableBubble.Raise();
        } else if (gameObject.tag == "Coworker")
        {
            theBubble.MakeActive();
        }

        hasTriggered = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !hasTriggered && isTrigger)
        {
            if (needSquare)
            {
                if (!thePC.isCircle)
                {
                    TriggerDialogue();
                    hasTriggered = true;
                }
            } else
            {
                TriggerDialogue();
                hasTriggered = true;
            }

        }
    }

    public void Stuck()
    {
        isStuck = true;
    }
}
